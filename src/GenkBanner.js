import { html, css, LitElement } from 'lit';

export class GenkBanner extends LitElement {
  static get styles() {
    return css`
      :host {
        --regular-font: 'Source Sans Pro', sans-serif;
        --white-color: #dddddd;
        --main-inner-shadow: inset 0 0 1rem var(--dark-main-color);
        --dark-main-color: #222831;
        display: block;
      }

      .banner__container {
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;
        text-align: center;
        width: 100%;
        height: 50vh;
        background-color: var(--white-color);
        box-shadow: var(--main-inner-shadow);
      }

      .banner__title {
        text-align: center;
        width: 100%;
        font-family: 'Catamaran', sans-serif;
        font-size: 5rem;
        background: linear-gradient(to right, #312dff 0%, #f05454 100%);
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;
        transform: translateX(250px);
        opacity: 0;
        transition: 0.7s all ease;
        position: relative;
      }

      .active {
        transform: translateX(0);
        opacity: 1;
      }
      .banner__subtitle {
        --regular-font: 'Rock Salt', cursive;
        --main-font-size: 1rem;
        color: var(--dark-main-color);
        cursor: pointer;
        font-size: var(--main-font-size);
        font-family: var(--regular-font);
      }

      .banner__subtitle:hover {
        animation: shake 0.5s;
        animation-iteration-count: infinite;
      }

      @media only screen and (max-width: 768px) {
        .banner__title {
          font-size: 2rem;
        }
        .banner__subtitle {
          --main-font-size: 0.8rem;
        }
      }

      @keyframes shake {
        0% {
          transform: translate(1px, 1px) rotate(0deg);
        }
        10% {
          transform: translate(-1px, -2px) rotate(-1deg);
        }
        20% {
          transform: translate(-3px, 0px) rotate(1deg);
        }
        30% {
          transform: translate(3px, 2px) rotate(0deg);
        }
        40% {
          transform: translate(1px, -1px) rotate(1deg);
        }
        50% {
          transform: translate(-1px, 2px) rotate(-1deg);
        }
        60% {
          transform: translate(-3px, 1px) rotate(0deg);
        }
        70% {
          transform: translate(3px, 1px) rotate(-1deg);
        }
        80% {
          transform: translate(-1px, -1px) rotate(1deg);
        }
        90% {
          transform: translate(1px, 2px) rotate(0deg);
        }
        100% {
          transform: translate(1px, -2px) rotate(-1deg);
        }
      }
    `;
  }

  static get properties() {
    return {
      title: {
        type: String,
      },
      subtitle: {
        type: String,
      },
    };
  }

  connectedCallback() {
    super.connectedCallback();
  }

  firstUpdated() {
    window.addEventListener('scroll', () => {
      const banner = this.shadowRoot.querySelector('.banner__title');
      banner.classList.toggle(
        'active',
        banner.getBoundingClientRect().top < window.innerHeight - 150
      );
    });
  }

  render() {
    return html`
      <section class="banner__container">
        <h1 class="banner__title">${this.title}</h1>
        <p class="banner__subtitle">${this.subtitle}</p>
      </section>
    `;
  }
}
